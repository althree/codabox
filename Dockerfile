FROM ubuntu:16.10

MAINTAINER Dockerfiles

# Install required packages and remove the apt packages cache when done.
RUN apt-get update && apt-get install -y \
	git \
	python \
	python-dev \
	python-setuptools \
	nginx \
	supervisor \
	sqlite3 \
	build-essential \
  && rm -rf /var/lib/apt/lists/*

RUN easy_install pip

# install uwsgi now because it takes a little while
RUN pip install uwsgi

# setup all the configfiles and code
COPY supervisor-codabox.conf /etc/supervisor/conf.d/supervisor-codabox.conf
RUN rm -v /etc/nginx/nginx.conf
COPY nginx-codabox.conf /etc/nginx/nginx.conf
COPY codabox /home/docker/code/app/
WORKDIR  /home/docker/code/app/
RUN pip install -r /home/docker/code/app/requirements.txt
RUN chmod +x start.sh

EXPOSE 80
CMD ["./start.sh"]
